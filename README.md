具体操作过程请参见：https://blog.csdn.net/u013007181/article/details/129972427

本仓库中的ansible剧本仅适用于内部网络，若要联网部署，可参考仓库：https://gitee.com/ptu706/k8s

内部离线部署时，需完成以下工作：

1、在内网中搭建私有镜像仓库(如Harbor)，并完成镜像推送，具体镜像及其版本可在代码所在目录执行以下命令查看：
【find . -type f -exec grep -H "192.168.18.18:9999/kube/" {} \;】  #分号前有一个反斜杠，上传时被过滤掉了，请自行添加

2、配置内部文件服务器，提供文件共享服务

3、在内网中配置yum软件仓库，并参见local_init/tasks/repo.yaml文件中配置openEuler软件仓库

4、将file/download.sh文件中要下载的软件包提前下载，并放置在内网文件服务器中共享，其中下载链接修改为自己的

5、根据自身情况修改配置文件，在代码所在目录执行以下命令查看需要修改的文件，将私有镜像仓库地址改为自己的。
【find . -type f -exec grep -H "192.168.18.18:9999" {} \;】   #分号前有一个反斜杠，上传时被过滤掉了，请自行添加

5、相似的命令，查看需要修改k8s节点IP地址的配置文件，并修改为自己的。

6、其它操作过程参见上面的博文

7、部署完成后，执行【kubectl get node】命令，若发现没有节点，或者少了节点(多次测试，偶尔有此现象，原因未知)，请重启k8s各节点，可能要重启多次。
