#!/bin/bash

if [ ! -f "runc.amd64" ];then
wget http://192.168.18.18/k8s/runc.amd64 
else
echo "文件存在"
fi

if [ ! -f "cni-plugins-linux-amd64-v1.1.1.tgz" ];then
wget http://192.168.18.18/k8s/cni-plugins-linux-amd64-v1.1.1.tgz
else
echo "文件存在"
fi

if [ ! -f "cri-containerd-cni-1.6.15-linux-amd64.tar.gz" ];then
wget http://192.168.18.18/k8s/cri-containerd-cni-1.6.15-linux-amd64.tar.gz
else
echo "文件存在"
fi

if [ ! -f "crictl-v1.26.0-linux-amd64.tar.gz" ];then
wget http://192.168.18.18/k8s/crictl-v1.26.0-linux-amd64.tar.gz
else
echo "文件存在"
fi

if [ ! -f "kubernetes-server-linux-amd64.tar.gz" ];then
wget http://192.168.18.18/k8s/kubernetes-server-linux-amd64.tar.gz
else
echo "文件存在"
fi

if [ ! -f "etcd-v3.5.6-linux-amd64.tar.gz" ];then
wget http://192.168.18.18/k8s/etcd-v3.5.6-linux-amd64.tar.gz
else
echo "文件存在"
fi

if [ ! -f "cfssl" ];then
wget http://192.168.18.18/k8s/cfssl
else
echo "文件存在"
fi

if [ ! -f "cfssljson" ];then
wget http://192.168.18.18/k8s/cfssljson
else
echo "文件存在"
fi

if [ ! -f "nginx-1.22.1.tar.gz" ];then
wget http://192.168.18.18/k8s/nginx-1.22.1.tar.gz
else
echo "文件存在"
fi

